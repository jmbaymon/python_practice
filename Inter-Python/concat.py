# names = [
# 'Jeff','fred','sam', 'Jill']
#
# # for name in names:
#     # print('Hello ' + name )
# print(', '.join(names)) # use join for more than one string

import os

location_of_files = '../'
file = 'write.py'

with open(os.path.join(location_of_files, file)) as f:
    print(f.read())
